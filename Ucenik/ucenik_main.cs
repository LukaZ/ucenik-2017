﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
////

using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Reflection;
using Ucenik.Properties;



namespace Ucenik
{
    public partial class ucenik_main : Form
    {
        DataSet table = new DataSet();
        DataSet tabelaRaspored = new DataSet();
        Data newData = new Data();
        
        public ucenik_main()
        {
            this.Icon = Properties.Resources.icon;
            
            InitializeComponent();
            Ucitaj_Sliku();

            newData.Ucitaj_Podatke(table,Data.dirObaveze, obaveze_datagrid);

            newData.Ucitaj_Podatke(tabelaRaspored, Data.dirTabelaRaspored, dataGridView1);

            comboBox();
            TipRasporeda(Settings.Default["tipRasporeda"].ToString());
            newData.proveriStanje(false,Sacuvaj);
            newData.proveriStanje(false, button5);


            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fileVersionInfo.ProductVersion;

            this.Text += " v" + version;

            
        }

        

        private void Ucitaj_Sliku()
        {

            //za sliku picturebox-a
            if (File.Exists(Data.dirRaspored))
            {
                this.rasporedpic.ImageLocation = Data.dirRaspored;
            }
            else
            {
                this.rasporedpic.Image = Ucenik.Properties.Resources.no_image;
            }
        }

        private void comboBox()
        {
            // Testiranje
            comboBox1.Items.Add("Slika");
            comboBox1.Items.Add("Tabela");
            comboBox1.SelectedIndex = comboBox1.FindStringExact(Settings.Default["tipRasporeda"].ToString());
        }

        private void Sacuvaj_Sliku()
        {

            OpenFileDialog slika = new OpenFileDialog();
            //slika.Filter = "Images | *.jpeg *.png *.bmp *.jpg";
            slika.Title = "Izaberite novi raspored";
            if (slika.ShowDialog() == DialogResult.OK)
            {

                this.rasporedpic.ImageLocation = slika.FileName;
                //Provera dali postoji folder 
                if (!Directory.Exists(Data.dirDefault))
                {
                    Directory.CreateDirectory(Data.dirDefault);
                }
                File.Copy(slika.FileName, Data.dirDefault + "/raspored.png", true);

            }

        }

        private void raspored_Click(object sender, EventArgs e)
        {
            Sacuvaj_Sliku();
            Ucitaj_Sliku();
        }

        private void dodaj_Click(object sender, EventArgs e)
        {
            newData.dodajRed(table, Sacuvaj);
        }

        public void Sacuvaj_Click(object sender,EventArgs e)
        {
            newData.sacuvajPromene(table, Sacuvaj , Data.dirObaveze);
        }

        private void Izbrisi_Click(object sender, EventArgs e)
        {
            newData.izbrisiRed(obaveze_datagrid,Sacuvaj);
        }

        private void Resetuj_Click(object sender, EventArgs e)
        {
            var izbor1 = MessageBox.Show("Ovo dugme ce IZBRISATI sve podatke u tabeli i postaviti ih na pocetnu vrednost ( 3 prazna polja).\nDa li ste sigurni da zelite da nastavite?", "Upozorenje", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if(izbor1 == DialogResult.OK)
            {     
                try
                {
                    File.Delete(Data.dirObaveze);
                    newData.Ucitaj_Podatke(table,Data.dirObaveze, obaveze_datagrid);
                    this.Close();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }            
            }
        }

        public void ToolStrip_Verzija_Click(object sender, EventArgs e)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fileVersionInfo.ProductVersion;

            MessageBox.Show("Izdanje(verzija) programa je "+version ,"Informacija",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }

        private void ToolStrip_Sugestija_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Svaka sugestija/predlog je dobrodosao,predlog mozete poslati na:\nlukazagar123@gmail.com","Sugestija",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }

        private void ToolStrip_Greska_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Ukoliko ste naisli na problem , prijavite slobodno na lukazagar123@gmail.com  pokusacu da odgovorim sto brze moguce :)", "Hvala", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void moj_dnevnik_Click(object sender, EventArgs e)
        {
            dnevnik dnevnik_form = new dnevnik();
            dnevnik_form.Show();
            
            
        }

        private void tabela_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            newData.proveriStanje(true, Sacuvaj);
        }

        private void ucenik_main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (newData.waitExit)
            {
                var result = MessageBox.Show("Niste sacuvali promene, da li zelite da nastavite ? ", "Upozorenje", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if(result == DialogResult.OK)
                {
                    this.Close();
                }
                else if( result == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        /// !!!
        private void ProveriRed(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == obaveze_datagrid.RowCount - 1)
            {
                this.IzbrisiRed.Enabled = false;
            }
            else
            {
                this.IzbrisiRed.Enabled = true;
            }
        }
        /// !!!

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString() == "Tabela")
            {
                Settings.Default["tipRasporeda"] = "Tabela";
            }
            else if (comboBox1.SelectedItem.ToString() == "Slika")
            {
                Settings.Default["tipRasporeda"] = "Slika";
            }
            Settings.Default.Save();
            TipRasporeda(Settings.Default["tipRasporeda"].ToString());
        }

        private void TipRasporeda(string raspored)
        {
            if(raspored == "Tabela")
            {
                /// Slika

                this.groupBoxSlika.Visible = false;               
                this.rasporedpic.Visible = false;
                
                /// Tabela

                this.dataGridView1.Visible = true;
                this.groupBoxTabela.Visible = true;
                
            }
            else if (raspored == "Slika")
            {
                /// Slika

                this.groupBoxSlika.Visible = true;
                this.rasporedpic.Visible = true;

                /// Tabela

                this.dataGridView1.Visible = false;
                this.groupBoxTabela.Visible = false;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Sigurno zelite da izbrisete raspored?", "Upozorenje", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(result == DialogResult.Yes)
            {
                File.Delete(Data.dirRaspored);
                Ucitaj_Sliku();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Process.Start(@Data.dirDefault);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            newData.dodajRed(tabelaRaspored, Sacuvaj);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            newData.izbrisiRed(dataGridView1,button5);
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Title = "Sacuvaj raspored od tabele";
            dlg.Filter = " XML File| *.xml ";
            dlg.FilterIndex = 1;
            if(dlg.ShowDialog() == DialogResult.OK)
            {
                tabelaRaspored.Tables[0].WriteXml(dlg.FileName);
            }
           
        }

        private void button5_Click(object sender, EventArgs e)
        {
            newData.sacuvajPromene(tabelaRaspored, button5 ,Data.dirTabelaRaspored);
        }

        private void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            newData.proveriStanje(true, button5);
        }
    }
}
