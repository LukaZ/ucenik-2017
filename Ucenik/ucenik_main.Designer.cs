﻿namespace Ucenik
{
    partial class ucenik_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucenik_main));
            this.rasporedpic = new System.Windows.Forms.PictureBox();
            this.raspored = new System.Windows.Forms.Button();
            this.obaveze_datagrid = new System.Windows.Forms.DataGridView();
            this.dodaj = new System.Windows.Forms.Button();
            this.Sacuvaj = new System.Windows.Forms.Button();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.IzbrisiRed = new System.Windows.Forms.Button();
            this.Resetuj = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.posaljiSugestijuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prijaviGreskuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moj_dnevnik = new System.Windows.Forms.Button();
            this.groupBoxSlika = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBoxTabela = new System.Windows.Forms.GroupBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.rasporedpic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obaveze_datagrid)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBoxSlika.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBoxTabela.SuspendLayout();
            this.SuspendLayout();
            // 
            // rasporedpic
            // 
            this.rasporedpic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rasporedpic.Location = new System.Drawing.Point(12, 30);
            this.rasporedpic.Name = "rasporedpic";
            this.rasporedpic.Size = new System.Drawing.Size(558, 347);
            this.rasporedpic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.rasporedpic.TabIndex = 0;
            this.rasporedpic.TabStop = false;
            // 
            // raspored
            // 
            this.raspored.Location = new System.Drawing.Point(6, 23);
            this.raspored.Name = "raspored";
            this.raspored.Size = new System.Drawing.Size(307, 45);
            this.raspored.TabIndex = 1;
            this.raspored.Text = "Promeni sliku za raspored";
            this.raspored.UseVisualStyleBackColor = true;
            this.raspored.Click += new System.EventHandler(this.raspored_Click);
            // 
            // obaveze_datagrid
            // 
            this.obaveze_datagrid.AllowUserToResizeColumns = false;
            this.obaveze_datagrid.AllowUserToResizeRows = false;
            this.obaveze_datagrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.obaveze_datagrid.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.obaveze_datagrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.obaveze_datagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.obaveze_datagrid.Location = new System.Drawing.Point(582, 30);
            this.obaveze_datagrid.Name = "obaveze_datagrid";
            this.obaveze_datagrid.Size = new System.Drawing.Size(547, 486);
            this.obaveze_datagrid.TabIndex = 2;
            this.obaveze_datagrid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.tabela_CellBeginEdit);
            // 
            // dodaj
            // 
            this.dodaj.Location = new System.Drawing.Point(6, 27);
            this.dodaj.Name = "dodaj";
            this.dodaj.Size = new System.Drawing.Size(229, 30);
            this.dodaj.TabIndex = 3;
            this.dodaj.Text = "Dodaj red";
            this.dodaj.UseVisualStyleBackColor = true;
            this.dodaj.Click += new System.EventHandler(this.dodaj_Click);
            // 
            // Sacuvaj
            // 
            this.Sacuvaj.Location = new System.Drawing.Point(241, 27);
            this.Sacuvaj.Name = "Sacuvaj";
            this.Sacuvaj.Size = new System.Drawing.Size(100, 30);
            this.Sacuvaj.TabIndex = 4;
            this.Sacuvaj.Text = "Sacuvaj promene";
            this.Sacuvaj.UseVisualStyleBackColor = true;
            this.Sacuvaj.Click += new System.EventHandler(this.Sacuvaj_Click);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(343, 381);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 5;
            // 
            // IzbrisiRed
            // 
            this.IzbrisiRed.Location = new System.Drawing.Point(347, 27);
            this.IzbrisiRed.Name = "IzbrisiRed";
            this.IzbrisiRed.Size = new System.Drawing.Size(100, 30);
            this.IzbrisiRed.TabIndex = 6;
            this.IzbrisiRed.Text = "Izbrisi red";
            this.IzbrisiRed.UseVisualStyleBackColor = true;
            this.IzbrisiRed.Click += new System.EventHandler(this.Izbrisi_Click);
            // 
            // Resetuj
            // 
            this.Resetuj.Location = new System.Drawing.Point(453, 27);
            this.Resetuj.Name = "Resetuj";
            this.Resetuj.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Resetuj.Size = new System.Drawing.Size(92, 30);
            this.Resetuj.TabIndex = 7;
            this.Resetuj.Text = "Resetuj tabelu";
            this.Resetuj.UseVisualStyleBackColor = true;
            this.Resetuj.Click += new System.EventHandler(this.Resetuj_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1135, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.versionToolStripMenuItem,
            this.posaljiSugestijuToolStripMenuItem,
            this.prijaviGreskuToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.optionsToolStripMenuItem.Text = "File";
            // 
            // versionToolStripMenuItem
            // 
            this.versionToolStripMenuItem.Name = "versionToolStripMenuItem";
            this.versionToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.versionToolStripMenuItem.Text = "Version";
            this.versionToolStripMenuItem.Click += new System.EventHandler(this.ToolStrip_Verzija_Click);
            // 
            // posaljiSugestijuToolStripMenuItem
            // 
            this.posaljiSugestijuToolStripMenuItem.Name = "posaljiSugestijuToolStripMenuItem";
            this.posaljiSugestijuToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.posaljiSugestijuToolStripMenuItem.Text = "Posalji sugestiju";
            this.posaljiSugestijuToolStripMenuItem.Click += new System.EventHandler(this.ToolStrip_Sugestija_Click);
            // 
            // prijaviGreskuToolStripMenuItem
            // 
            this.prijaviGreskuToolStripMenuItem.Name = "prijaviGreskuToolStripMenuItem";
            this.prijaviGreskuToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.prijaviGreskuToolStripMenuItem.Text = "Prijavi gresku";
            this.prijaviGreskuToolStripMenuItem.Click += new System.EventHandler(this.ToolStrip_Greska_Click);
            // 
            // moj_dnevnik
            // 
            this.moj_dnevnik.Location = new System.Drawing.Point(343, 550);
            this.moj_dnevnik.Name = "moj_dnevnik";
            this.moj_dnevnik.Size = new System.Drawing.Size(227, 30);
            this.moj_dnevnik.TabIndex = 10;
            this.moj_dnevnik.Text = "Moj Dnevnik";
            this.moj_dnevnik.UseVisualStyleBackColor = true;
            this.moj_dnevnik.Click += new System.EventHandler(this.moj_dnevnik_Click);
            // 
            // groupBoxSlika
            // 
            this.groupBoxSlika.Controls.Add(this.button2);
            this.groupBoxSlika.Controls.Add(this.button1);
            this.groupBoxSlika.Controls.Add(this.raspored);
            this.groupBoxSlika.Location = new System.Drawing.Point(12, 409);
            this.groupBoxSlika.Name = "groupBoxSlika";
            this.groupBoxSlika.Size = new System.Drawing.Size(319, 176);
            this.groupBoxSlika.TabIndex = 12;
            this.groupBoxSlika.TabStop = false;
            this.groupBoxSlika.Text = "Kontrole za raspored";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(6, 123);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(307, 45);
            this.button2.TabIndex = 3;
            this.button2.Text = "Otvori folder od rasporeda";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 74);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(307, 45);
            this.button1.TabIndex = 2;
            this.button1.Text = "Izbrisi sliku rasporeda";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(130, 390);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Tip rasporeda";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(208, 387);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(123, 21);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dodaj);
            this.groupBox2.Controls.Add(this.Sacuvaj);
            this.groupBox2.Controls.Add(this.Resetuj);
            this.groupBox2.Controls.Add(this.IzbrisiRed);
            this.groupBox2.Location = new System.Drawing.Point(578, 522);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(551, 63);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Kontrole za tabelu sa obavezama";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 30);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(558, 347);
            this.dataGridView1.TabIndex = 14;
            this.dataGridView1.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridView1_CellBeginEdit);
            // 
            // groupBoxTabela
            // 
            this.groupBoxTabela.Controls.Add(this.button6);
            this.groupBoxTabela.Controls.Add(this.button5);
            this.groupBoxTabela.Controls.Add(this.button4);
            this.groupBoxTabela.Controls.Add(this.button3);
            this.groupBoxTabela.Location = new System.Drawing.Point(12, 409);
            this.groupBoxTabela.Name = "groupBoxTabela";
            this.groupBoxTabela.Size = new System.Drawing.Size(319, 176);
            this.groupBoxTabela.TabIndex = 15;
            this.groupBoxTabela.TabStop = false;
            this.groupBoxTabela.Text = "Kontrole za raspored";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(162, 23);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(151, 45);
            this.button6.TabIndex = 3;
            this.button6.Text = "Izbrisi red";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(6, 74);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(307, 45);
            this.button5.TabIndex = 2;
            this.button5.Text = "Sacuvaj raspored";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(6, 125);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(307, 45);
            this.button4.TabIndex = 1;
            this.button4.Text = "Arhiviraj raspored";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(6, 23);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(151, 45);
            this.button3.TabIndex = 0;
            this.button3.Text = "Dodaj red";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // ucenik_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1135, 590);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.obaveze_datagrid);
            this.Controls.Add(this.rasporedpic);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.moj_dnevnik);
            this.Controls.Add(this.groupBoxTabela);
            this.Controls.Add(this.groupBoxSlika);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "ucenik_main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ucenik";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ucenik_main_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.rasporedpic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obaveze_datagrid)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBoxSlika.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBoxTabela.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox rasporedpic;
        private System.Windows.Forms.Button raspored;
        public System.Windows.Forms.DataGridView obaveze_datagrid;
        private System.Windows.Forms.Button dodaj;
        private System.Windows.Forms.Button Sacuvaj;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Button IzbrisiRed;
        private System.Windows.Forms.Button Resetuj;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem posaljiSugestijuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prijaviGreskuToolStripMenuItem;
        private System.Windows.Forms.Button moj_dnevnik;
        private System.Windows.Forms.GroupBox groupBoxSlika;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBoxTabela;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button6;
    }
}

