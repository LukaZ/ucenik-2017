﻿namespace Ucenik
{
    partial class dnevnik
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetujTabeluSaOcenamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.erorListaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dnevnik_datagrid = new System.Windows.Forms.DataGridView();
            this.Dodaj = new System.Windows.Forms.Button();
            this.SacuvajPromene = new System.Windows.Forms.Button();
            this.IzbrisiRed = new System.Windows.Forms.Button();
            this.ArhivirajOcene = new System.Windows.Forms.Button();
            this.UcitajTabelu = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dnevnik_datagrid)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.erorListaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(764, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetujTabeluSaOcenamaToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // resetujTabeluSaOcenamaToolStripMenuItem
            // 
            this.resetujTabeluSaOcenamaToolStripMenuItem.Name = "resetujTabeluSaOcenamaToolStripMenuItem";
            this.resetujTabeluSaOcenamaToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.resetujTabeluSaOcenamaToolStripMenuItem.Text = "Resetuj tabelu sa ocenama";
            this.resetujTabeluSaOcenamaToolStripMenuItem.Click += new System.EventHandler(this.ToolStrip_Resetuj_Click);
            // 
            // erorListaToolStripMenuItem
            // 
            this.erorListaToolStripMenuItem.Name = "erorListaToolStripMenuItem";
            this.erorListaToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.erorListaToolStripMenuItem.Text = "Eror lista";
            this.erorListaToolStripMenuItem.Click += new System.EventHandler(this.erorListaToolStripMenuItem_Click);
            // 
            // dnevnik_datagrid
            // 
            this.dnevnik_datagrid.AllowUserToResizeColumns = false;
            this.dnevnik_datagrid.AllowUserToResizeRows = false;
            this.dnevnik_datagrid.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dnevnik_datagrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dnevnik_datagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dnevnik_datagrid.Location = new System.Drawing.Point(0, 24);
            this.dnevnik_datagrid.Name = "dnevnik_datagrid";
            this.dnevnik_datagrid.Size = new System.Drawing.Size(764, 563);
            this.dnevnik_datagrid.TabIndex = 2;
            this.dnevnik_datagrid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dnevnik_datagrid_CellBeginEdit);
            this.dnevnik_datagrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dnevnik_datagrid_CellEndEdit);
            this.dnevnik_datagrid.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ProveriRed);
            // 
            // Dodaj
            // 
            this.Dodaj.Location = new System.Drawing.Point(6, 655);
            this.Dodaj.Name = "Dodaj";
            this.Dodaj.Size = new System.Drawing.Size(230, 39);
            this.Dodaj.TabIndex = 3;
            this.Dodaj.Text = "Dodaj novi red";
            this.Dodaj.UseVisualStyleBackColor = true;
            this.Dodaj.Click += new System.EventHandler(this.Dodaj_Click);
            // 
            // SacuvajPromene
            // 
            this.SacuvajPromene.Location = new System.Drawing.Point(242, 655);
            this.SacuvajPromene.Name = "SacuvajPromene";
            this.SacuvajPromene.Size = new System.Drawing.Size(134, 39);
            this.SacuvajPromene.TabIndex = 4;
            this.SacuvajPromene.Text = "Sacuvaj promene";
            this.SacuvajPromene.UseVisualStyleBackColor = true;
            this.SacuvajPromene.Click += new System.EventHandler(this.SacuvajPromene_Click);
            // 
            // IzbrisiRed
            // 
            this.IzbrisiRed.Location = new System.Drawing.Point(386, 655);
            this.IzbrisiRed.Name = "IzbrisiRed";
            this.IzbrisiRed.Size = new System.Drawing.Size(119, 39);
            this.IzbrisiRed.TabIndex = 5;
            this.IzbrisiRed.Text = "Izbrisi red";
            this.IzbrisiRed.UseVisualStyleBackColor = true;
            this.IzbrisiRed.Click += new System.EventHandler(this.IzbrisiRed_Click);
            // 
            // ArhivirajOcene
            // 
            this.ArhivirajOcene.Location = new System.Drawing.Point(636, 655);
            this.ArhivirajOcene.Name = "ArhivirajOcene";
            this.ArhivirajOcene.Size = new System.Drawing.Size(119, 39);
            this.ArhivirajOcene.TabIndex = 6;
            this.ArhivirajOcene.Text = "Sacuvaj ocene";
            this.ArhivirajOcene.UseVisualStyleBackColor = true;
            this.ArhivirajOcene.Click += new System.EventHandler(this.ArhivirajOcene_Click);
            // 
            // UcitajTabelu
            // 
            this.UcitajTabelu.Location = new System.Drawing.Point(511, 655);
            this.UcitajTabelu.Name = "UcitajTabelu";
            this.UcitajTabelu.Size = new System.Drawing.Size(119, 39);
            this.UcitajTabelu.TabIndex = 7;
            this.UcitajTabelu.Text = "Ucitaj tabelu";
            this.UcitajTabelu.UseVisualStyleBackColor = true;
            this.UcitajTabelu.Click += new System.EventHandler(this.UcitajTabelu_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox1.Location = new System.Drawing.Point(6, 593);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(230, 54);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Prvo polugodiste";
            // 
            // textBox1
            // 
            this.textBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox1.Location = new System.Drawing.Point(130, 19);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(63, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Prosek iz svih predmeta:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox2.Location = new System.Drawing.Point(242, 593);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(230, 54);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Drugo polugodiste";
            // 
            // textBox2
            // 
            this.textBox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox2.Location = new System.Drawing.Point(134, 19);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(63, 20);
            this.textBox2.TabIndex = 2;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Prosek iz svih predmeta:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox3);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox3.Location = new System.Drawing.Point(478, 593);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(274, 54);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Kraj godine";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Control;
            this.textBox3.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox3.Location = new System.Drawing.Point(180, 19);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(63, 20);
            this.textBox3.TabIndex = 2;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Prosek sa trenutnim ocenama :";
            // 
            // dnevnik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 699);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.UcitajTabelu);
            this.Controls.Add(this.ArhivirajOcene);
            this.Controls.Add(this.IzbrisiRed);
            this.Controls.Add(this.SacuvajPromene);
            this.Controls.Add(this.Dodaj);
            this.Controls.Add(this.dnevnik_datagrid);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "dnevnik";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Moj Dnevnik";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.dnevnik_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dnevnik_datagrid)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.DataGridView dnevnik_datagrid;
        private System.Windows.Forms.Button Dodaj;
        private System.Windows.Forms.Button SacuvajPromene;
        private System.Windows.Forms.Button IzbrisiRed;
        private System.Windows.Forms.Button ArhivirajOcene;
        private System.Windows.Forms.Button UcitajTabelu;
        private System.Windows.Forms.ToolStripMenuItem resetujTabeluSaOcenamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem erorListaToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;
    }
}