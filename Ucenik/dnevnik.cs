﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using System.Diagnostics;
using System.IO;
using System.Xml.Linq;

namespace Ucenik
{
    public partial class dnevnik : Form
    {

        DataSet dataset = new DataSet();
        Data newData = new Data();
        
        public bool ReqSaving = false;
      
        public dnevnik()
        {
            InitializeComponent();
            this.Icon = Properties.Resources.icon;
            
            newData.Ucitaj_Podatke(dataset,Data.dirDnevnik, dnevnik_datagrid);
            Proveri_Stanje();
            Prosek(1, 2, textBox1);
            Prosek(3, 4, textBox2);
            Formatiranje_Tabele();
        }
        
        private void Formatiranje_Tabele()
        {
            textBox3.BackColor = Color.Khaki; 
            foreach (DataGridViewColumn col in dnevnik_datagrid.Columns)
            {
                if (col.Name == "PrvoPolugodiste" || col.Name == "DrugoPolugodiste")
                {
                    try
                    {
                        col.Width = 210;



                        dnevnik_datagrid.Columns[1].HeaderCell.Value = "Prvo polugodiste";
                        dnevnik_datagrid.Columns[3].HeaderCell.Value = "Drugo polugodiste";

                        dnevnik_datagrid.Columns[2].HeaderCell.Value = "Ocena\nza prvo polugodiste";
                        dnevnik_datagrid.Columns[2].DefaultCellStyle.BackColor = Color.Khaki;
                        dnevnik_datagrid.Columns[2].ReadOnly = true; /// Ogranicavanje na taj red

                        dnevnik_datagrid.Columns[4].HeaderCell.Value = "Ocena\nza drugo polugodiste";
                        dnevnik_datagrid.Columns[4].DefaultCellStyle.BackColor = Color.Khaki;
                        dnevnik_datagrid.Columns[4].ReadOnly = true; /// Ogranicavanje na taj red

                        dnevnik_datagrid.Columns[5].HeaderCell.Value = "Ocena iz\n predmeta";
                        dnevnik_datagrid.Columns[5].DefaultCellStyle.BackColor = Color.Khaki;
                        dnevnik_datagrid.Columns[5].ReadOnly = true; /// Ogranicavanje na taj red

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                if (col.Name == "ProsekPrvoPolugodiste" || col.Name == "ProsekDrugoPolugodiste" || col.Name == "OcenaIzPredmeta")
                {
                    try
                    {
                        col.Width = 67;
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }

        public void Dodaj_Click(object sender, EventArgs e)
        {
            dataset.Tables[0].Rows.Add();
            ReqSaving = true;
            Proveri_Stanje();
        }

        private void SacuvajPromene_Click(object sender, EventArgs e)
        {
            Sacuvaj();
        }

        private void Sacuvaj()
        {
            dataset.Tables[0].WriteXml(Data.dirDnevnik);
            ReqSaving = false;
            Proveri_Stanje();
        }

        private void ArhivirajOcene_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "XML File (*.xml)|*.xml";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                dataset.Tables[0].WriteXml(dlg.FileName);
            }

        }

        private void dnevnik_datagrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            ReqSaving = true;
            Proveri_Stanje();
        }

        private void IzbrisiRed_Click(object sender, EventArgs e)
        {
            try
            {
                int rowcount = 0;

                //Prebroji koliko redova imamo u tabeli
                foreach (DataGridViewRow row in dnevnik_datagrid.Rows)
                {
                    rowcount++;
                }
                //Ukloni red/ove koji su selektovani
                foreach (DataGridViewRow row in this.dnevnik_datagrid.SelectedRows)
                {
                    dnevnik_datagrid.Rows.RemoveAt(row.Index);
                    ReqSaving = true;
                    SacuvajPromene.Enabled = true;
                }
                //Ako je broj redova ostao nepromenjen nakon operacije uklanjanja redova prikazati poruku kako ukloniti red
                if (dnevnik_datagrid.RowCount == rowcount)
                {
                    MessageBox.Show("Uklanjanje redova se vrsi tako sto se stisne krajnji levi jezicak kod zeljenog reda za brisanje", "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void ToolStrip_Resetuj_Click(object sender, EventArgs e)
        {
            var izbor1 = MessageBox.Show("Ovo dugme ce IZBRISATI sve podatke u tabeli i postaviti ih na pocetnu vrednost ( 3 prazna polja).\nDa li ste sigurni da zelite da nastavite?", "Upozorenje", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (izbor1 == DialogResult.OK)
            {
                var izbor2 = MessageBox.Show("Pritiskom dugmeta OK oznacavate tabelu za brisanje svih podataka , nastaviti ? \n Napomena:Brisanje podataka ce ugasiti trenutno otvoreni program.", "Upozorenje", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (izbor2 == DialogResult.Yes)
                {
                    try
                    {
                        File.Delete(Data.dirDnevnik);
                        
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }

        private void UcitajTabelu_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "XML File (*.xml)|*.xml";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                dataset.Tables[0].ReadXml(dlg.FileName);
            }
        }

        public void Prosek(int inputRow , int outputRow , TextBox inputTextbox)
        {
            int prosekSum = 0, validRowCount = 0;
            double prosekSum2 = 0;

            for (int i = 0; i <= dnevnik_datagrid.Rows.Count - 1; ++i)
            {
                double prosek = 0;
                int t = 0, sum = 0, integer;

                switch(newData.ValidateRow(i, inputRow, outputRow, dnevnik_datagrid))
                {
                    case 1:
                        string data = dnevnik_datagrid.Rows[i].Cells[inputRow].Value.ToString();
                        data = data.Replace(",", "");

                        validRowCount++;
                        integer = Convert.ToInt32(data);
                        int duzina = integer.ToString().Length, i1 = 0;

                        for (i1 = 0; i1 < duzina; i1++)
                        {
                            t = integer % 10;
                            sum += t;
                            integer = integer / 10;
                        }

                        prosek = (double)sum / duzina;
                        dnevnik_datagrid.Rows[i].Cells[outputRow].Value = prosek;
                        
                        prosekSum += (int)Math.Round(prosek, MidpointRounding.AwayFromZero);
                       
                        break;
                    case 999:
                        dnevnik_datagrid.Rows[i].Cells[outputRow].Value = 999;
                        break;
                    case 998:
                        dnevnik_datagrid.Rows[i].Cells[outputRow].Value = 998;
                        break;
                }

                if (inputTextbox != null && i == validRowCount)
                {
                    inputTextbox.Text = ((double)prosekSum / validRowCount).ToString();
                }
            }
            for(int i = 0; i < validRowCount; i++) 
            {
                double prosek = 0;
                if(newData.isNotEmpty(i,2,dnevnik_datagrid) && newData.isNotEmpty(i, 4, dnevnik_datagrid))
                {
                    prosek = ((Convert.ToDouble(dnevnik_datagrid.Rows[i].Cells["ProsekPrvoPolugodiste"].Value)) + (Convert.ToDouble(dnevnik_datagrid.Rows[i].Cells["ProsekDrugoPolugodiste"].Value))) / 2.0;
                    if(prosek <= 5.0)
                    {
                        dnevnik_datagrid.Rows[i].Cells["OcenaIzPredmeta"].Value = prosek;
                        prosekSum2 += prosek;
                    }
                    else
                    {
                        dnevnik_datagrid.Rows[i].Cells["OcenaIzPredmeta"].Value = "";
                    }
                    
                } 
            }
            if((prosekSum2 / validRowCount) > 5.0)
            {
                this.textBox3.Text = "NaN";
            }
            else
            {
                this.textBox3.Text = (prosekSum2 / validRowCount).ToString();
            }

        }

        private void erorListaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("999 - Celija sadrzi nepodrzavane karaktere ( podrzavani su 12345 i ,)\n998 - Celija sadrzi predugacak niz ocena", "Eror lista", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void dnevnik_datagrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Prosek(1, 2, textBox1);
            Prosek(3, 4, textBox2);
        }

        private void ProveriRed(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == dnevnik_datagrid.RowCount - 1)
            {
                this.IzbrisiRed.Enabled = false;
            }
            else
            {
                this.IzbrisiRed.Enabled = true;
            }
        }
        

        private void Proveri_Stanje()
        {
            if (ReqSaving == true)
            {
                SacuvajPromene.Enabled = true;
            }
            else if (ReqSaving == false)
            {
                SacuvajPromene.Enabled = false;
            }
        }

        private void dnevnik_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ReqSaving)
            {
                var result = MessageBox.Show("Niste sacuvali promene u tabeli sa ocenama , da li zelite da sacuvate promene? ", "Upozorenje", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    Sacuvaj();
                    ReqSaving = false;
                }
                else if (result == DialogResult.No)
                {
                    ReqSaving = false;
                    this.Close();
                }
                else if (result == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

       
    }

}
