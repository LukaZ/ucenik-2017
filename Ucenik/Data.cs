﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;


///
using System.Configuration;

namespace Ucenik
{
    public class Data
    {
        public static string dirDefault = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Ucenik");
        public static string dirObaveze = (dirDefault + "/obaveze.xml");
        public static string dirDnevnik = (dirDefault + "/dnevnik.xml");
        public static string dirRaspored = (dirDefault + "/raspored.png");
        public static string dirTabelaRaspored = (dirDefault + "/tabela.xml");

        public bool waitExit = false; 
        

        public void Ucitaj_Podatke(DataSet table, string dirTable, DataGridView targetDataGrid)
        {

            if (File.Exists(Data.dirDnevnik) && File.Exists(Data.dirObaveze) && File.Exists(Data.dirTabelaRaspored))
            {
                try
                {
                    table.ReadXml(dirTable);
                    targetDataGrid.DataSource = table.Tables[0];

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                Directory.CreateDirectory(Data.dirDefault);

                /// Za raspored
                XDocument obavezexml =
                   new XDocument(
                     new XElement("Tabela",
                       new XElement("Obaveza",
                            new XElement("Predmet"),
                            new XElement("Opis"),
                            new XElement("Datum")),
                       new XElement("Obaveza",
                            new XElement("Predmet"),
                            new XElement("Opis"),
                            new XElement("Datum")
                            )
                     )
                   );
                obavezexml.Save(Data.dirObaveze);
                // Za dnevnik
                XDocument dnevnikxml =
                      new XDocument(
                       new XElement("Tabela",
                         new XElement("PredmetRed",
                              new XElement("Predmet"),
                              new XElement("PrvoPolugodiste"),
                              new XElement("ProsekPrvoPolugodiste"),
                              new XElement("DrugoPolugodiste"),
                              new XElement("ProsekDrugoPolugodiste"),
                              new XElement("OcenaIzPredmeta"))
                      )
                   );
                dnevnikxml.Save(Data.dirDnevnik);
                
                XDocument rasporedkxml =
                      new XDocument(
                       new XElement("Tabela",
                         new XElement("CasRed",
                              new XElement("Ponedeljak"),
                              new XElement("Utorak"),
                              new XElement("Sreda"),
                              new XElement("Cetvrtak"),
                              new XElement("Petak"))
                      )
                   );
                rasporedkxml.Save(Data.dirTabelaRaspored);

                Ucitaj_Podatke(table, dirTable, targetDataGrid);
            }
        }

        public void izbrisiRed(DataGridView dgv , Button dgvSaveButton)
        {
            try
            {
                int rowcount = 0;
                
                //Prebroji koliko redova imamo u tabeli
                foreach (DataGridViewRow row in dgv.Rows)
                {
                    rowcount++;
                }
                //Ukloni red/ove koji su selektovani
                foreach (DataGridViewRow row in dgv.SelectedRows)
                {
                    dgv.Rows.RemoveAt(row.Index);
                    proveriStanje(true, dgvSaveButton);
                }
                //Ako je broj redova ostao nepromenjen nakon operacije uklanjanja redova prikazati poruku kako ukloniti red
                if (dgv.RowCount == rowcount)
                {
                    MessageBox.Show("Uklanjanje redova se vrsi tako sto se stisne krajnji levi jezicak kod zeljenog reda za brisanje", "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void dodajRed(DataSet dataset,Button saveButton)
        {
            dataset.Tables[0].Rows.Add();
            proveriStanje(true,saveButton);
        }

        public void sacuvajPromene(DataSet table , Button saveButton , string saveDir)
        {
            try
            {
                table.Tables[0].WriteXml(saveDir);
                proveriStanje(false, saveButton);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void proveriStanje(bool ReqSaving , Button saveButton)
        {
            if (ReqSaving == true)
            {
                saveButton.Enabled = true;
                waitExit = true;
            }
            else if (ReqSaving == false)
            {
                saveButton.Enabled = false;
                waitExit = false;
            }
        }

        public int ValidateRow(int currentRow, int inputRow, int outputRow, DataGridView datagrid)
        {
            if (isNotEmpty(currentRow,inputRow,datagrid))
            {
                if (datagrid.Rows[currentRow].Cells[inputRow].Value.ToString().All(c => "12345,".Contains(c)))
                {
                    string data = datagrid.Rows[currentRow].Cells[inputRow].Value.ToString();
                    data = data.Replace(",", "");

                    if (data.Length < 10)
                    {
                        return 1;
                    }
                    else
                    {
                        return 998;
                    }
                }
                else
                {
                    return 999;
                }
            }
            else
            {
                return 0;
            }
        }

        public bool isNotEmpty(int currentRow, int rowToCheck, DataGridView datagrid)
        {
            if (datagrid.Rows[currentRow].Cells[rowToCheck].Value != null && datagrid.Rows[currentRow].Cells[rowToCheck].Value.ToString() != "")
            {
                return true;
            }
            else return false;

        }
    }
}
